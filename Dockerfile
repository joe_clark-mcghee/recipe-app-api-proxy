FROM nginxinc/nginx-unprivileged:1-alpine

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# Change to root user to create files and change permissions before switching back to NGINX user.
USER root
RUN mkdir -p /vol/static
# Change permission of the static dir to allow the NGINX user to read files.
RUN chmod 755 /vol/static
# Create and empty txt file that the output of the `envsubst` command points to. Change the
# owner to NGINX. We need to do this because the start up script is run by the NGINX user. The
# NGINX does not have permissions to create a file so we have to create an empty one that can be
# edited.
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]
