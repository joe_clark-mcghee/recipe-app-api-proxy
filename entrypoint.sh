#!/bin/sh

# If any part of the start up script fails, terminate the script.
set -e

# Populate env vars in default.conf.tpl and move to default nginx config file location
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Start the nginx service and run in the foreground by disabling the deamon.
nginx -g 'daemon off;'